import datetime
import math
import os

from google.cloud import bigquery
import requests

BASE_URL = 'https://statsapi.web.nhl.com'

PATH = '/home/brent/Downloads/nhl-data-1d7275266d3e.json'
DATASET = 'nhl_data'

if 'GOOGLE_APPLICATION_CREDENTIALS' in os.environ:
    pass
else:
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = PATH
client = bigquery.Client()
dataset_ref = client.dataset(DATASET)
events_id = 'events_table_20182019'
events_ref = dataset_ref.table(events_id)
events_table = client.get_table(events_ref)

players_id = 'players_table_20182019'
players_ref = dataset_ref.table(players_id)
players_table = client.get_table(players_ref)

def main(game_date):
    game_data = fetch_game_data(game_date)
    if game_data:
        for game in game_data:
            events, players = structure_content(*game)
            num_events_batches = math.ceil(len(events)/500)
            num_players_batches = math.ceil(len(players)/500)
            for i in range(num_events_batches):
                if i == num_events_batches - 1:
                    events_batches = events[i*500:]
                    errors = client.insert_rows(events_table, events_batches)
                    assert errors == []
                else:
                    events_batches = events[i*500:(i+1)*500]
                    errors = client.insert_rows(events_table, events_batches)
                    assert errors == []
                print('Events Table Loaded!')
            for i in range(num_players_batches):
                if i == num_players_batches - 1:
                    players_batches = players[i*500:]
                    errors = client.insert_rows(players_table, players_batches)
                    assert errors == []
                else:
                    players_batches = players[i*500:(i+1)*500]
                    errors = client.insert_rows(players_table, players_batches)
                    assert errors == []
                print('Players Table Loaded!')        

def fetch_game_data(game_date):
    data = requests.get('{}/api/v1/schedule?date={}'.format(BASE_URL, game_date)).json()
    if data['dates']:
        games = data['dates'][0]['games']
        game_data = []
        for game in games:
            teams = game['teams']
            game_data.append(
                (
                    game['link'], 
                    str(game['gamePk']), 
                    str(game['gamePk'])[4:6],
                    teams['home']['team']['name'],
                    str(teams['home']['team']['id']),
                    teams['away']['team']['name'],
                    str(teams['away']['team']['id'])
                )
            )
        return game_data
    else:
        return 

def structure_content(game_link, game_id, game_type, home_team_name, home_team_id, away_team_name, away_team_id):
    data = requests.get(BASE_URL + game_link).json()
    events_table_rows = []
    players_table_rows = []
    for event in data['liveData']['plays']['allPlays']:
        about = event['about']
        event_id = str(about['eventId'])
        away_goals = about['goals']['away']
        home_goals = about['goals']['home']
        ordinal_num = about['ordinalNum']
        period_time = about['periodTime']
        period_time_remaining = about['periodTimeRemaining']
        period_type = about['periodType']

        result = event['result']
        description = result['description']
        event_ = result['event']
        event_code = result['eventCode']

        if 'secondaryType' in result:
            secondary_type = result['secondaryType']
        else:
            secondary_type = None

        if 'team' in event:
            team_id = str(event['team']['id'])
            if 'triCode' in event['team']:
                team_tri_code = event['team']['triCode']
            else:
                team_tri_code = None 
        else:
            team_id = None
            team_tri_code = None

        if event['coordinates']:
            if 'x' in event['coordinates'] and 'y' in event['coordinates']:
                x_coord = event['coordinates']['x']
                y_coord = event['coordinates']['y']
        else:
            x_coord = None
            y_coord = None
        
        event_row_tuple = (
            event_id,
            game_id,
            game_type,
            home_team_name,
            home_team_id,
            away_team_name,
            away_team_id,
            team_id,
            away_goals,
            home_goals,
            ordinal_num,
            period_time,
            period_time_remaining,
            period_type,
            description,
            event_,
            event_code,
            secondary_type,
            x_coord,
            y_coord,
            team_tri_code
        )
        events_table_rows.append(event_row_tuple)

        if 'players' in event:
            for player in event['players']:
                player_id = str(player['player']['id'])
                full_name = player['player']['fullName']
                player_type = player['playerType']
                player_row_tuple = (
                    event_id,
                    game_id,
                    player_id,
                    full_name,
                    player_type
                )
                players_table_rows.append(player_row_tuple)
    return events_table_rows, players_table_rows

if __name__ == "__main__":
    start = datetime.datetime.strptime("2018-09-01", "%Y-%m-%d")
    end = datetime.datetime.strptime("2019-04-12", "%Y-%m-%d")
    date_generated = [start + datetime.timedelta(days=x) for x in range(0, (end-start).days)]
    dates = [date.strftime("%Y-%m-%d") for date in date_generated]
    main('2019-04-12')
    # for date in dates:
    #     main(date)
        # game_data = fetch_game_data(date)
        # if game_data:
        #     for game in game_data:
        #         print(game)
        #         events, players = structure_content(*game)

        # main(date)
    # events, players = structure_content(r'/api/v1/game/2018021270/feed/live')
    # print(len(events), len(players))