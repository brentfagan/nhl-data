import os

from google.cloud import bigquery

def main():
    PATH = '/home/brent/Downloads/nhl-data-1d7275266d3e.json'
    DATASET = 'nhl_data'

    if 'GOOGLE_APPLICATION_CREDENTIALS' in os.environ:
        pass
    else:
        os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = PATH

    client = bigquery.Client()
    dataset_ref = client.dataset(DATASET)

    # events table
    events_schema = [
        bigquery.SchemaField('event_id', 'STRING', mode='REQUIRED'),
        bigquery.SchemaField('game_id', 'STRING', mode='REQUIRED'),
        bigquery.SchemaField('game_type', 'STRING', mode='REQUIRED'),
        bigquery.SchemaField('home_team', 'STRING', mode='REQUIRED'),
        bigquery.SchemaField('home_team_id', 'STRING', mode='REQUIRED'),
        bigquery.SchemaField('away_team', 'STRING', mode='REQUIRED'),
        bigquery.SchemaField('away_team_id', 'STRING', mode='REQUIRED'),
        bigquery.SchemaField('event_team_id', 'STRING'),
        bigquery.SchemaField('away_goals', 'INTEGER'),
        bigquery.SchemaField('home_goals', 'INTEGER'),
        bigquery.SchemaField('ordinal_num', 'STRING'),
        bigquery.SchemaField('period_time', 'STRING'),
        bigquery.SchemaField('period_time_remaining', 'STRING'),
        bigquery.SchemaField('period_type', 'STRING'),
        bigquery.SchemaField('description', 'STRING'),
        bigquery.SchemaField('event', 'STRING'),
        bigquery.SchemaField('event_code', 'STRING'),
        bigquery.SchemaField('secondary_type', 'STRING'),
        bigquery.SchemaField('x_coord', 'FLOAT'),
        bigquery.SchemaField('y_coord', 'FLOAT'),
        bigquery.SchemaField('event_team_tri_code', 'STRING'),
    ]

    events_table_ref = dataset_ref.table('events_table_20182019')
    try:
        client.get_table(events_table_ref).num_rows
    except:
        events_table = bigquery.Table(events_table_ref, schema=events_schema)
        events_table = client.create_table(events_table)

    # players table
    players_schema = [
        bigquery.SchemaField('event_id', 'STRING', mode='REQUIRED'),
        bigquery.SchemaField('game_id', 'STRING', mode='REQUIRED'),
        bigquery.SchemaField('player_id', 'STRING', mode='REQUIRED'),
        bigquery.SchemaField('full_name', 'STRING'),
        bigquery.SchemaField('player_type', 'STRING'),
    ]

    players_table_ref = dataset_ref.table('players_table_20182019')
    try:
        client.get_table(players_table_ref).num_rows
    except:
        players_table = bigquery.Table(players_table_ref, schema=players_schema)
        players_table = client.create_table(players_table)

if __name__ == "__main__":
    main()